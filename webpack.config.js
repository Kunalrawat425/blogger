

const path = require('path');
const { VueLoaderPlugin } = require('vue-loader')

module.exports = {
    // This is the "main" file which should include all other modules
    entry: './resources/js/app.js',
    // Where should the compiled file go?
    output: {
        path: path.resolve(__dirname, './public/js/'),
        publicPath: path.resolve(__dirname, './public/js/'),
        filename: 'vue-bundle.js'
    },
    module: {
      rules: [
        {
            // vue-loader config to load `.vue` files or single file components.
            test: /\.vue$/,
            loader: 'vue-loader',
            options: {
                loaders: {
                    // https://vue-loader.vuejs.org/guide/scoped-css.html#mixing-local-and-global-styles
                    css: ['vue-style-loader', {
                        loader: 'css-loader',
                    }],
                    js: [
                        'babel-loader',
                    ],
                },
                cacheBusting: true,
            },
        },
        {
            // This is required for other javascript modules you are gonna write besides vue.
            test: /\.js$/,
            loader: 'babel-loader',
            include: [
                path.resolve(__dirname, 'src'),
                path.resolve(__dirname, 'node_modules/webpack-dev-server/client'),
            ],
        },
      ]
   },
   resolve: {
      alias: {
        'vue$': 'vue/dist/vue.esm.js'
      },
      extensions: ['*', '.js', '.vue', '.json']
    },
    plugins: [
            // make sure to include the plugin for the magic
            new VueLoaderPlugin()
        ]
    ,

}