
<html>
    <head>
        <title>Bloggers <?= (!empty($title)) ? " - ".$title : "" ?></title>
    <style>
    body{
        font-family: 'Poppins';
    }
    a{
        text-decoration:unset !important;
    }
    .cursor
        {
            cursor:pointer;
        }
    </style>
        
    </head>
    @include('layouts.header')

    <body>
    
        @include('layouts.navbar')
        <br>
        <div class="uk-container">
            @yield('content')
        </div>

        @include('layouts.footer')
    </body>
</html>