<div class="uk-width-1-3@m">
    <div class="uk-margin-medium-bottom">
        <p class="uk-text-large"><?=$article->articlecategory->name?></p>
        <button class="uk-button uk-button-primary uk-button-small">Follow</button>
    </div>
    <?php if($relatedArticles):?>
    <div class="uk-margin-medium-bottom">
        <h4>Related Topics<hr></h4>
        <?php foreach($relatedArticles as $article): ?>
            <div class="uk-text-lead uk-text-truncate"><?=$article->title?></div>
            <div class="uk-text-meta uk-text-truncate"><?= ($article->description) ? $article->description : ''?></div>
            <div class="uk-margin">
            <div class="uk-text-small"><?= $article->user->username?></div>
            <div class="uk-text-small"> <?= $article->updated_at->format('d M Y') ?> </div>
        <?php endforeach;?>

    </div>
    <?php endif;?>
    <?php if($popularArticles):?>
    <div class="uk-margin-medium-bottom">
        <h4>Popular Topics<hr></h4>
        <?php foreach($popularArticles as $article): ?>
            <div class="uk-text-lead uk-text-truncate"><?=$article->title?></div>
            <div class="uk-text-meta uk-text-truncate"><?= ($article->description) ? $article->description : ''?></div>
            <div class="uk-margin">
            <div class="uk-text-small"><?= $article->user->username?></div>
            <div class="uk-text-small"> <?= $article->updated_at->format('d M Y') ?> </div>
        <?php endforeach;?>

    </div>
    <?php endif;?>
</div>