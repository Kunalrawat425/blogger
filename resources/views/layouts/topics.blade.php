@extends('master')

@section('content')
<br>
<h1>Explore Topics</h1><hr><br>
<div class="uk-child-width-1-4@m uk-grid-small " uk-grid>
    <?php foreach($allTopics as $allTopic):?>
    <div class="cursor">
        <div class="uk-card uk-card-default">
            <div class="uk-card-media-top">
                <img src="<?= $allTopic->img_url ?>" alt="">
            </div>
            <div class="uk-card-body uk-card-small">
                <h3 class="uk-card-title "><?=$allTopic->name?></h3>
            </div>
        </div>
    </div>
    <?php endforeach;?>
</div>

@endsection