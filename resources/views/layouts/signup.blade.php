@extends('master')

@section('content')
<br>

<div class="uk-grid-collapse uk-child-width-1-2@s uk-flex-middle" uk-grid>
    <div class="uk-background-cover" style="background-image: url('https://i.graphicmama.com/blog/wp-content/uploads/2018/06/18083812/Flat-Design-Character-modern-colors-illustration.jpg');" uk-height-viewport></div>
    <div class="uk-padding-large">
            <div id="app">
                <signup></signup>
            </div>
    </div>
</div>

@endsection
