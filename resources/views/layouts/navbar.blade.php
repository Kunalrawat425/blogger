<style>
.logo{}
</style>
<div class="uk-section-primary uk-preserve-color">

    <div uk-sticky="animation: uk-animation-slide-top; sel-target: .uk-navbar-container; cls-active: uk-navbar-sticky; cls-inactive: uk-navbar-transparent uk-light; top: 100">

        <nav class="uk-navbar uk-container" uk-navbar="dropbar: true; dropbar-mode: push" >
            <div class="uk-navbar-left">

                <ul class="uk-navbar-nav ">
                    <li class="uk-active"><a href="<?=action('ArticleCategoriesController@index');?>" class="uk-text-large logo">Medium</a></li>
                </ul>

            </div>
            <div class="uk-navbar-right">

                <ul class="uk-navbar-nav ">

                    <?php if(Auth::user()):?>
                        <li><a href="#"><?= Auth::user()->username ?></a></li>
                        <div uk-dropdown="mode: click">
                        <ul class="uk-nav uk-dropdown-nav">
                            <li><a href="<?=action('ArticlesController@userArticles');?>">My Articles</a></li>
                            <li><a href="<?=action('Auth\LoginController@logout');?>">Logout</a></li>
                        </ul>
                    </div>
                        <li><a href="<?=action('ArticlesController@topics');?>">Topics</a></li>
                    <?php else:?>
                        <li ><a href="<?=action('Auth\LoginController@login');?>">Log in</a></li>
                        <li ><a href="<?=action('Auth\RegisterController@signup');?>">Sign up </a></li>
                    <?php endif;?>

                    <li>
                        <a href="<?=action('ArticlesController@add');?>">
                            <button class="uk-button uk-button-primary " >Get started</button>
                        </a>
                    </li>

                </ul>

            </div>

        </nav>
    </div>
</div>
