@extends('master')


@section('content')
<div uk-grid>
    @include('layouts.article-home-left-bar')
    

    @include('layouts.article-home-right-bar')
</div>

@endsection