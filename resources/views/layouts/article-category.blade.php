<div uk-grid>
    <div class="uk-width-expand">
        <?php foreach($articleCategories as $articleCategory): ?>

                <h1 class="uk-h2"><?=$articleCategory->name?><hr></h1>
                <?php if(!empty($articleCategory->article)):?>
                    <?php foreach($articleCategory->article as $article): ?>
                        <a href="<?=action('ArticlesController@view',['title'=>$article->title,'id'=>$article->id]);?>">
                            <div class="uk-card uk-card-hover uk-card-default uk-grid-collapse  uk-margin" uk-grid>
                                <div class="uk-card-body uk-width-expand uk-card-small">
                                    <div class="uk-text-lead"><?=$article->title?></div>
                                    <div class="uk-text-meta uk-text-truncate"><?= ($article->description) ? $article->description : ''?></div>
                                    <div class="uk-margin">
                                    <div class="uk-text-small"><?= $article->user->username?></div>
                                    <div class="uk-text-small"> <?= $article->updated_at->format('d M Y') ?> </div>
                                    </div>
                                </div>
                                <div class="uk-card-media-right uk-width-1-4" style="background:url('https://www.gettyimages.com/gi-resources/images/CreativeLandingPage/HP_Sept_24_2018/CR3_GettyImages-159018836.jpg');background-size:cover;background-position:center;">
                                </div>
                            </div>
                        </a>
                    <?php endforeach;?>
                <?php else:?>
                    No Articles Yet
                <?php endif;?>

        <?php endforeach;?>
    </div>
    <div class="uk-width-1-3">
    <h4>Popular on Medium<hr></h4>
        <?php foreach($popularArticles as $popularArticle): ?>
            <a href="<?=action('ArticlesController@view',['title'=>$popularArticle->title,'id'=>$popularArticle->id]);?>">
            <div class="uk-card-body  uk-padding-remove uk-margin">

            <div class="uk-text-lead"><?=$article->title?></div>
            <div class="uk-text-meta uk-text-truncate"><?= ($article->description) ? $article->description : ''?></div>
            <div class="uk-margin">
            <div class="uk-text-small"><?= $article->user->username?></div>
            <div class="uk-text-small"> <?= $article->updated_at->format('d M Y') ?> </div>
            </div>
            </a>
        <?php endforeach;?>
    </div>
</div>
