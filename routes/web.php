<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','ArticleCategoriesController@index');

Route::get('/topics','ArticlesController@topics');

Route::get('/{title}-{id}','ArticlesController@view')->where(['id' => '[0-9]+']);
Route::get('/my-articles','ArticlesController@userArticles');


Route::post('/signup','Auth\RegisterController@signup');
Route::get('/signup','Auth\RegisterController@signup');


Route::post('/login','Auth\LoginController@login')->name('login');
Route::get('/login','Auth\LoginController@login');

Route::get('/logout','Auth\LoginController@logout');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/add-article','ArticlesController@add');
Route::post('/add-article','ArticlesController@add');

Route::get('/get-article-categories','ArticleCategoriesController@getArticleCategories');

Route::post('/get-comments/{id}','CommentsController@getComments');

