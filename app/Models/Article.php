<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Article extends Model
{
    public function ArticleCategory(){
        return $this->belongsTo('App\ArticleCategory');
    }

    public function Comment(){
        return $this->hasMany('App\Comment');
    }

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }


    public function getAllArticles(){
        $articles = Article::all();
        return $articles;
    }

    public function getSingleArticle($id){
        $article = Article::with('Comment','Comment.CommentReply','ArticleCategory')->find((int) $id);
        return $article;
    }

    public function getPopularArticles($id=null){
        if($id){
            $article_category_id = Article::getSingleArticle($id)->article_category_id;

            $popularArticles = Article::where('article_category_id',$article_category_id)
                                        ->orderBy('view_counts', 'desc')
                                        ->get();

        }else{
            $popularArticles = Article::orderBy('view_counts', 'desc')
                                        ->take(4)
                                        ->get();
        }
    
        return $popularArticles;
    }

    public function getRelatedArticles($id){

        $article_category_id = Article::getSingleArticle($id)->article_category_id;
        $relatedArticles = Article::where('article_category_id',$article_category_id)
                                    ->take(4)
                                    ->get();
        return $relatedArticles;
    }

    public function getUserArticles($user_id){

        $articles = Article::where('user_id',$user_id)
                                    ->get();
        return $articles;
    }
    


    
}
