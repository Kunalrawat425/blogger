<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticleCategory extends Model
{
    public function article(){
        return $this->hasMany('App\Article','article_category_id');
    }

    public function getAllArticlesByCategory(){
        
        $articles = ArticleCategory::with('article','article.user')->get();

        return $articles;

    }

    public function getAllArticleCategories(){
        
        $articles = ArticleCategory::get();

        return $articles;

    }

}
