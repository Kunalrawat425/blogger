<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Comment;

class Comment extends Model
{
    public function CommentReply() {
        return $this->hasMany('App\CommentReply');
    }

    public function getComments($id){

        $comments = Comment::with('commentreply')->where('article_id',$id)->get();

        return $comments;

    }
}
