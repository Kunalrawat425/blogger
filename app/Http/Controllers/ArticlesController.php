<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Comment;
use App\ArticleCategory;
use Auth;

class ArticlesController extends Controller
{

    public function __construct()
    {
        // Middleware only applied to these methods
        $this->middleware('auth', ['only' => ['add'] ]);    
    }   

    public function index(){

        $articles = new Article();

        return view('home',['articles'=>$articles->getAllArticles()]);
        
    }

    public function popularArticles(){

        $articles = new Article();

        return view('home',['articles'=>$articles->getPopularArticles()]);
        
    }

    public function view($title,$id){
        $article = new Article();

        return view('layouts.article-home',['article'=>$article->getSingleArticle($id),'relatedArticles'=>$article->getRelatedArticles($id),'popularArticles'=>$article->getPopularArticles($id)]);
        
    }

    public function topics(){

        $topic = new ArticleCategory();

        $allTopics = $topic->getAllArticleCategories();
  
        return view('layouts.topics',['allTopics'=>$allTopics]);
    }

    public function userArticles(){

        $userId = Auth::id();
        
        $article = new Article();

        return view('layouts.user-articles',['articles'=>$article->getUserArticles($userId)]);
    }

    public function add(Request $request){
        
        if($request->isMethod('post')){

            $article = new Article();

            $article->title = $request->title;
            $article->description = $request->description;
            $article->content = $request->content;
            $article->article_category_id = $request->article_category_id;
            $article->user_id = 1;

            if($article->save()){
                $status=true;
            }else{
                $status=false;
            }

            return response()->json(['saved'=>$status]);
            
        }else{

            $articleCategories = new ArticleCategory();

            $articleCategories = $articleCategories->getAllArticleCategories();

            return view('layouts.add-article',['articleCategories'=>$articleCategories]);
        }
    }
}
