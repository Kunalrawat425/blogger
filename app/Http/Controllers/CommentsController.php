<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;

class CommentsController extends Controller
{
    public function getComments($id){

        $commentTable = new Comment();

        $comments = $commentTable->getComments((int) $id);

        return response()->json(['comments'=>$comments]);

    }
}
