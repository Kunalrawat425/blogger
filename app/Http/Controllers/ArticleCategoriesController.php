<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ArticleCategory;
use App\Article;

class ArticleCategoriesController extends Controller
{
    public function index(){

        $articleCategoriesTable = new ArticleCategory();

        $articleCategories = $articleCategoriesTable->getAllArticlesByCategory();

        $articles = new Article();

        $popularArticles = $articles->getPopularArticles();

        return view('home',['articleCategories'=>$articleCategories,'popularArticles'=>$popularArticles]);
    }


    public function getArticleCategories(){

        $articleCategories = new ArticleCategory();

        $articleCategories = $articleCategories->getAllArticleCategories();

        return response()->json(['articleCategories'=>$articleCategories]);

    }

}
