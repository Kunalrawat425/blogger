<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request) {

        if($request->isMethod('post')){
            $data=$request->all();
        
            $userExists = User::where('email',$data['username'])->orWhere('username',$data['username'])->first();
            if(empty($userExists)){
                return response()->json(['user'=>false]);
            }
            else{
                if(Auth::attempt(['email' =>$data['username'] , 'password' => $data['password'] ])){

                    $status="success";
    
                }else{

                    $status="failed";
    
                }
                return response()->json(['saved'=>$status]);
            }

        }
        else{
            
            return view('layouts.login',['title'=>'Get valuable resources']);
        }


    }
    public function logout(){

        Auth::logout();
        return redirect('/');
    }
}
